CC=g++
SOURCES=./src/main.cpp
EXECUTABLE=./build/2048
SFML_HEADER_DIR=./lib/SFML/include
SFML_LIB_DIR=./lib/SFML/lib
SFML_LIBS=-lsfml-graphics-s -lsfml-window-s -lsfml-system-s -lopengl32 -lwinmm -lgdi32 -lfreetype

all: 
	$(CC) $(SOURCES) -o $(EXECUTABLE) -DSFML_STATIC -I $(SFML_HEADER_DIR) -L $(SFML_LIB_DIR) $(SFML_LIBS)
	
	 
run:
	${EXECUTABLE}
	