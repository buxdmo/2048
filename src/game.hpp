
#ifndef GAME_HPP
#define GAME_HPP

#include <stdlib.h>
#include <ctime>


static constexpr int FIELD_SIZE = 4;
static constexpr int START_COUNT = 2;
static constexpr int WIN_VALUE = 2048;

sf::Vector2i operator!(const sf::Vector2i& v)
{
    return sf::Vector2i(abs(v.y), abs(v.x));
}

sf::Vector2i operator*(const sf::Vector2i& v)
{
    return sf::Vector2i(
        v.x < 0 ? 0 : v.x,
        v.y < 0 ? 0 : v.y
    );
}


struct Tile 
{
    sf::Vector2i pos {0, 0};
    sf::Vector2i prevPos {0, 0};
    int value {0};
    bool animMove {0};
};


class Game 
{
public:
    Tile field[FIELD_SIZE][FIELD_SIZE]{ };

    int score = 0;
    bool isPlaying = true;
    bool win    = false;
    bool change = false;

public:
    Game() 
    {
        srand(time(0));
        static int row, col;
        for (size_t i = 0; i < START_COUNT; i++)
        {
            do
            {
                row = rand() % FIELD_SIZE;
                col = rand() % FIELD_SIZE;
            } while (field[row][col].value != 0);
            
            field[row][col].value = 2;
        }
    }

    void move(const sf::Vector2i& d) 
    {
        static sf::Vector2i a, b; 
        static sf::Vector2i ppd = !d;
        change = false;

        for (int i = 0; i < FIELD_SIZE; i++)
        {
            for (int j = 0; j < FIELD_SIZE; j++)
            {
                field[i][j].prevPos = field[i][j].pos;
                field[i][j].animMove = true;
            }
        }

        _move(d);

        for (int i = 0; i < FIELD_SIZE; i++)
        {
            a = ppd * i + (*d) * (FIELD_SIZE - 1);

            for (int j = 0; j < FIELD_SIZE; j++)
            {
                b = a - d;
                if(field[a.y][a.x].value != 0 &&
                   field[a.y][a.x].value == field[b.y][b.x].value)
                {
                    field[a.y][a.x].value *= 2;
                    field[b.y][b.x].value = 0;
                    field[a.y][a.x].prevPos = field[b.y][b.x].prevPos;
                    field[a.y][a.x].animMove = false;
                    if(field[a.y][a.x].value == WIN_VALUE)
                    {
                        isPlaying = false;
                        win = true;
                    }
                    score += field[a.y][a.x].value;
                    change = true;
                    --elementsCount;
                    ++j;
                    a -= d;
                }
                a -= d;
            }
            
        }
        _move(d);

        for (int i = 0; i < FIELD_SIZE; i++)
        {
            for (int j = 0; j < FIELD_SIZE; j++)
            {
                field[i][j].pos.y = i;
                field[i][j].pos.x = j;
            }
        }

        static int row, col; 
        if(change)
        {
            do
            {
                row = rand() % FIELD_SIZE;
                col = rand() % FIELD_SIZE;
            } while (field[row][col].value != 0);
            
            field[row][col].value = (rand() % 100 < 90) ? 2 : 4;
            field[row][col].animMove = false;

            ++elementsCount;

            if(elementsCount >= FIELD_SIZE * FIELD_SIZE)
            {
                bool isMoveAvaliable = false;
                for (int i = 0; i < FIELD_SIZE; i++)
                {
                    for (int j = 0; j < FIELD_SIZE; j++)
                    {
                        if( (i - 1 >= 0 && field[i][j].value == field[i - 1][j].value) ||
                            (j - 1 >= 0 && field[i][j].value == field[i][j - 1].value) ||
                            (i + 1 < FIELD_SIZE && field[i][j].value == field[i + 1][j].value) ||
                            (j + 1 < FIELD_SIZE && field[i][j].value == field[i][j + 1].value))
                        {
                            isMoveAvaliable = true;
                            break;
                        }
                    }
                    if(!isMoveAvaliable)
                    {
                        isPlaying = false;
                        win = false;
                    }
                }
                
            }
        }

    }

private:
    void _move(const sf::Vector2i& d) 
    {
        static sf::Vector2i a, b; 
        static sf::Vector2i ppd = !d;
        for (int i = 0; i < FIELD_SIZE; i++)
        {
            a = ppd * i + (*d) * (FIELD_SIZE - 1);

            for (int j = 0; j < FIELD_SIZE; j++)
            {
                if(field[a.y][a.x].value == 0) 
                {
                    b = a - d;
                    for (int k = j + 1; k < FIELD_SIZE; k++)
                    {
                        if (field[b.y][b.x].value != 0)
                        {
                            field[a.y][a.x].value = field[b.y][b.x].value;
                            field[b.y][b.x].value = 0;
                            field[a.y][a.x].prevPos = field[b.y][b.x].pos;
                            field[a.y][a.x].animMove = field[b.y][b.x].animMove;
                            change = true;
                            break;
                        }
                        b-=d;
                    }
                    
                }
                a -= d;
            }
            
        }
        
    }

private:
    int elementsCount = START_COUNT;
};

#endif //GAME_HPP