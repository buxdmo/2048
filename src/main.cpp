#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include <cmath>
#include "./game.hpp"


int main()
{
    static constexpr float WINDOW_SIZE = 400.0f;
    static constexpr float WINDOW_MARGIN = 100.0f;
    static constexpr int FIELD_SIZE = 4;
    static constexpr int TILE_PADDING = 5;
    static constexpr float TILE_SIZE = WINDOW_SIZE / FIELD_SIZE;
    static constexpr int ANIM_TIME = 144;
    static const sf::Vector2i up(0, -1), down(0, 1), left(-1, 0), right(1, 0);
    static const sf::Color colors[11] 
    {
        sf::Color(250, 231, 224),
        sf::Color(249, 225, 206),
        sf::Color(249, 179, 124),
        sf::Color(246, 149, 96),
        sf::Color(243, 125, 95),
        sf::Color(254, 92,  65),
        sf::Color(245, 208, 119),
        sf::Color(249, 208, 103),
        sf::Color(249, 202, 88),
        sf::Color(239, 196, 65),
        sf::Color(244, 198, 42)
    };

    float animCoef = 0;
    bool animating = false;
    sf::RectangleShape tile;
    Tile t;

    sf::Font font;
    font.loadFromFile("./src/fonts/arial.ttf");
    sf::Text text;
    text.setFont(font);
    text.setStyle(sf::Text::Bold);
    text.setCharacterSize(30);
    
    Game game;

    sf::RenderWindow window(
        sf::VideoMode(WINDOW_SIZE, WINDOW_SIZE + WINDOW_MARGIN),
        "2048"
    );

    while (window.isOpen())
    {    
        game.change = false;
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::KeyPressed:
                if(game.isPlaying && !animating)
                    switch (event.key.code)
                    {
                    case sf::Keyboard::Up: game.move(up); break;
                    case sf::Keyboard::Down: game.move(down); break;
                    case sf::Keyboard::Left: game.move(left); break;
                    case sf::Keyboard::Right: game.move(right); break;
                    }
                else if(!game.isPlaying && !animating &&
                        event.key.code == sf::Keyboard::Space) 
                {
                    game = Game(); 
                    animating = false;
                    animCoef = 0;
                }         
                break;
            }
        }
        

        if(game.change)
        {
            animating = true;
            animCoef = ANIM_TIME;
        }
        if(animating)
        {
            animCoef -= 0.6;
            if(animCoef <= 0)
                animating = false;
        }


        window.clear(sf::Color(250, 248, 239));

        tile.setOrigin(0, 0);
        tile.setSize(sf::Vector2f(WINDOW_SIZE, WINDOW_SIZE));
        tile.setPosition(0, WINDOW_MARGIN);
        tile.setFillColor(sf::Color(166, 148, 130));
        window.draw(tile);

        tile.setSize(
            sf::Vector2f(
                TILE_SIZE - TILE_PADDING * 2,
                TILE_SIZE - TILE_PADDING * 2
            )
        );
        tile.setFillColor(sf::Color(191, 177, 161));

        for (size_t i = 0; i < FIELD_SIZE; i++)
        {
            for (size_t j = 0; j < FIELD_SIZE; j++)
            {
                tile.setPosition(
                    j * TILE_SIZE + TILE_PADDING,
                    i * TILE_SIZE + TILE_PADDING + WINDOW_MARGIN
                );
                window.draw(tile);
            }   
        }
        
        for (size_t i = 0; i < FIELD_SIZE; i++)
        {
            for (size_t j = 0; j < FIELD_SIZE; j++)
            {
                t = game.field[i][j];

                if(t.value != 0) 
                {

                    tile.setSize(
                        sf::Vector2f(
                            TILE_SIZE - TILE_PADDING * 2,
                            TILE_SIZE - TILE_PADDING * 2
                        )
                    );
                    tile.setOrigin(tile.getSize() / 2.0f);

                    if(!animating)
                    {
                        tile.setFillColor(colors[(int)log2(game.field[i][j].value) - 1]);
                        tile.setPosition(
                            j * TILE_SIZE + TILE_SIZE / 2.0f,
                            i * TILE_SIZE + TILE_SIZE / 2.0f + WINDOW_MARGIN
                        ); 
                    }
                    else if(t.animMove)
                    {
                        tile.setFillColor(colors[(int)log2(game.field[i][j].value) - 1]);
                        tile.setPosition(
                            ((t.pos.x - t.prevPos.x) * (ANIM_TIME - animCoef) / ANIM_TIME + t.prevPos.x) * TILE_SIZE + TILE_SIZE / 2.0f,
                            ((t.pos.y - t.prevPos.y) * (ANIM_TIME - animCoef) / ANIM_TIME + t.prevPos.y) * TILE_SIZE + TILE_SIZE / 2.0f + WINDOW_MARGIN
                        ); 
                    }
                    else
                    {
                        tile.setFillColor(colors[(int)log2(game.field[i][j].value) - 2]);
                        tile.setPosition(
                            ((t.pos.x - t.prevPos.x) * (ANIM_TIME - animCoef) / ANIM_TIME + t.prevPos.x) * TILE_SIZE + TILE_SIZE / 2.0f,
                            ((t.pos.y - t.prevPos.y) * (ANIM_TIME - animCoef) / ANIM_TIME + t.prevPos.y) * TILE_SIZE + TILE_SIZE / 2.0f + WINDOW_MARGIN
                        );
                        window.draw(tile);
                        tile.setOrigin(tile.getSize() / 2.0f);
                        tile.setSize(
                            sf::Vector2f(
                                (TILE_SIZE - TILE_PADDING * 2) * (1.0f - animCoef / ANIM_TIME / 2.0f),
                                (TILE_SIZE - TILE_PADDING * 2) * (1.0f - animCoef / ANIM_TIME / 2.0f)
                            )
                        );
                        tile.setFillColor(colors[(int)log2(game.field[i][j].value) - 1]);
                        tile.setPosition(
                            j * TILE_SIZE + TILE_SIZE / 2.0f,
                            i * TILE_SIZE + TILE_SIZE / 2.0f + WINDOW_MARGIN
                        ); 
                    }

                    
                    window.draw(tile);

                    text.setFillColor(
                        game.field[i][j].value <= 4 ?
                        sf::Color(121, 112, 100) :
                        sf::Color::White
                    );
                    text.setPosition(tile.getPosition());
                    text.setString(std::to_string(game.field[i][j].value));
                    text.setOrigin(
                        text.getLocalBounds().width / 2.0f + text.getLocalBounds().left,
                        text.getLocalBounds().height / 2.0f + text.getLocalBounds().top
                    );
                    window.draw(text);
                }
            }
        }
        text.setFillColor(sf::Color(121, 112, 100));
        text.setPosition(20, 30);
        text.setString(std::to_string(game.score));
        text.setOrigin(0, 0);
        window.draw(text);
        

        if(game.win)
        {
            tile.setOrigin(0, 0);
            tile.setSize(sf::Vector2f(WINDOW_SIZE, WINDOW_SIZE));
            tile.setPosition(0, WINDOW_MARGIN);
            tile.setFillColor(sf::Color(166, 148, 130));
            window.draw(tile);

            text.setFillColor(sf::Color::White);
            text.setString("You win!\nSpace - to restart");
            text.setOrigin(
                text.getLocalBounds().width / 2.0f + text.getLocalBounds().left,
                text.getLocalBounds().height / 2.0f + text.getLocalBounds().top
            );
            text.setPosition(
                WINDOW_SIZE / 2.0f,
                WINDOW_SIZE / 2 + WINDOW_MARGIN
            );

            window.draw(text);
        }
        else if(!game.isPlaying)
        {
            tile.setOrigin(0, 0);
            tile.setSize(sf::Vector2f(WINDOW_SIZE, WINDOW_SIZE));
            tile.setPosition(0, WINDOW_MARGIN);
            tile.setFillColor(sf::Color(255,255,255,100));
            window.draw(tile);

            text.setFillColor(sf::Color(121,112,100));
            text.setString("Game Over!\nSpace - to restart");
            text.setOrigin(
                text.getLocalBounds().width / 2.0f + text.getLocalBounds().left,
                text.getLocalBounds().height / 2.0f + text.getLocalBounds().top
            );
            text.setPosition(
                WINDOW_SIZE / 2.0f,
                WINDOW_SIZE / 2 + WINDOW_MARGIN
            );

            window.draw(text);
        }

        window.display();
    }

    
    return 0;
}